<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([[
            'title' => 'a',
            'autor' => 'ava',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'ba',
            'autor' => 'bab',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'c',
            'autor' => 'bab',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'd',
            'autor' => 'caad',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'avar',
            'autor' => 'caad',
            'created_at' => date('Y-m-d G:i:s'),
        ]
        ]
        );  
        
    }
}